import React,{ useState, useEffect} from "react";
import axios from 'axios';
import "./App.css";

function App() {
  const [searchQuery, setSearchQuery] = useState('');
  const [responseItems, setResponseItems] = useState({ itemList: [] });
  const [error, setError] = useState();

  useEffect(() => {
    let ignore = false;

    async function fetchData() {
    try {
        const response = await axios ('http://localhost:8080/api/weather/cities/names?namesLike=' + searchQuery);
        if (!ignore) setResponseItems({itemList: response.data});
      } catch(error) {
        setError(error)
      }
    }

    fetchData();
    return () => { ignore = true; }
  }, [searchQuery])

    return (
      <div className="App">
        Enter keys to find Areas/cities Begins with <input value={searchQuery} type="text" maxlength="10" onChange={e => setSearchQuery(e.target.value)} />
           {responseItems.itemList.map((item) => {
                   return (
                       <h3>{item.name}</h3>
                       )}
                   )
                 }
        </div>
    );
}

export default App;
