# Coding assignment # 

A React app is created for UI
And backend is implemented as a spring boot app

To run the components,
    Prerequisites - Java 8 and Node

    ./weather-data-ui/npm start
    ./weather-data-api/gradlew bootRun

And the UI is accesible at http://localhost:3000/
