package com.weather.api.controller.handler;

import com.weather.api.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Slf4j
public class ExceptionHandler {
    @ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
    @org.springframework.web.bind.annotation.ExceptionHandler(ServiceException.class)
    public String handleError(ServiceException ex) {
        log.warn("Error fetching weather data", ex);
        return "Error fetching/preparing weather data, please try again";
    }
}
