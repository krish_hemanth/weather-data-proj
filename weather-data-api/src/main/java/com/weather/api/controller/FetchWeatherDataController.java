package com.weather.api.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.weather.api.exception.ServiceException;
import com.weather.api.model.Area;
import com.weather.api.service.FetchWeatherDataService;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.Length;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Slf4j
@Validated
public class FetchWeatherDataController {
    private FetchWeatherDataService fetchWeatherDataService;
    FetchWeatherDataController(FetchWeatherDataService fetchWeatherDataService) {
        this.fetchWeatherDataService = fetchWeatherDataService;
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = "/api/weather/cities/names", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Area> fetchAreaNames(@RequestParam(name ="namesLike") @Length(max=10) String beginsWith) throws ServiceException, JsonProcessingException {
        log.debug("fetchAreaNames api called");
        return fetchWeatherDataService.fetchAreaNamesBeginingWith(beginsWith);
    }
}
