package com.weather.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
//@Builder
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Area {
    private String id;
    private String name;
    private double latitude;
    private double longitude;

    private double temperature;
    private double temperatureMin;
    private double temperatureMax;

    @JsonProperty("coord")
    public void setCoord(Map<String, Double> coordinates) {
        this.latitude = coordinates.get("lat");
        this.longitude = coordinates.get("lon");
    }

    @JsonProperty("main")
    public void setMain(Map<String, Double> mainData) {
        this.temperature = mainData.get("temp");
        this.temperatureMin = mainData.get("temp_min");
        this.temperatureMax = mainData.get("temp_max");
    }
}
