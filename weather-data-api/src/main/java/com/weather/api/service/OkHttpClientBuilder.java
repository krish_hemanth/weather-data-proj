package com.weather.api.service;

import com.weather.api.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class OkHttpClientBuilder implements ClientBuilder {

    OkHttpClient client;
    OkHttpClientBuilder(@Value("${cache.dir}") String cacheDirectory) {
        File cacheLocation = new File(cacheDirectory, "http-cache");
        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(cacheLocation, cacheSize);
        client = new OkHttpClient.Builder().cache(cache).build();
    }

    @Override
    public String doGet(String url) throws ServiceException {
        Request request = new Request.Builder().cacheControl(
                new CacheControl.Builder().maxAge(300, TimeUnit.SECONDS).build())
                .url(url).build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            log.error("Error fetching data from url " + url);
            throw new ServiceException("Error fetching data from url " + url, e);
        }
    }
}
