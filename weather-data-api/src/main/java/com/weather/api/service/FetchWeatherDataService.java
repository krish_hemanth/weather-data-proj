package com.weather.api.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.api.exception.ServiceException;
import com.weather.api.model.Area;
import com.weather.api.model.WeatherData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FetchWeatherDataService {
    private ClientBuilder clientBuilder;

    @Value("${weather.data.url}")
    private String weatherDataUrl;

    FetchWeatherDataService(ClientBuilder clientBuilder) {
        this.clientBuilder = clientBuilder;
    }

    public List<Area> fetchAreaNamesBeginingWith(String beginsWith) throws ServiceException, JsonProcessingException {
        String response = clientBuilder.doGet(weatherDataUrl);
        ObjectMapper objectMapper = new ObjectMapper();

        WeatherData weatherData = objectMapper.readValue(response, WeatherData.class);
        List<Area> areaNames = weatherData.getAreasList().stream()
                .filter(area -> area.getName().toLowerCase().startsWith(beginsWith.toLowerCase()))
                .collect(Collectors.toList());
        return areaNames;
    }

}
