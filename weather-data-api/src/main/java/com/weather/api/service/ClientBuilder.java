package com.weather.api.service;

import com.weather.api.exception.ServiceException;

public interface ClientBuilder {
    String doGet(String url) throws ServiceException;
}
