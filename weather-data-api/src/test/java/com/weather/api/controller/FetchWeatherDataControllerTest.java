package com.weather.api.controller;

import com.weather.api.controller.handler.ExceptionHandler;
import com.weather.api.exception.ServiceException;
import com.weather.api.model.Area;
import com.weather.api.service.FetchWeatherDataService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class FetchWeatherDataControllerTest {

    @Mock
    private FetchWeatherDataService fetchWeatherDataService;

    @InjectMocks
    private FetchWeatherDataController fetchWeatherDataController;

    @Autowired
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(fetchWeatherDataController)
                .setControllerAdvice(new ExceptionHandler())
                .build();
    }

    @Test
    public void testGetWeatherNames_withOutRequestParam() throws Exception {
        //when
        this.mockMvc.perform(get("/api/weather/cities/names")
                .contentType(MediaType.APPLICATION_JSON))

                //then
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(status().reason("Required String parameter 'namesLike' is not present"));
        verify(fetchWeatherDataService,never()).fetchAreaNamesBeginingWith(anyString());
    }

    @Test
    public void testGetWeatherNames_success() throws Exception {
        //Given
        when(fetchWeatherDataService.fetchAreaNamesBeginingWith(anyString())).thenReturn(Arrays.asList(prepareArea()));

        //when
        MvcResult result = this.mockMvc.perform(get("/api/weather/cities/names?namesLike=t")
                .contentType(MediaType.APPLICATION_JSON))

                //then
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertThat(result.getResponse().getContentAsString()).isEqualTo("[{\"id\":\"1333\",\"name\":\"Test\",\"latitude\":0.0,\"longitude\":0.0,\"temperature\":13.0,\"temperatureMin\":0.0,\"temperatureMax\":0.0}]");
        verify(fetchWeatherDataService).fetchAreaNamesBeginingWith("t");
    }

    @Test
    public void testGetWeatherNames_errorRetrievingData() throws Exception {
        //Given
        when(fetchWeatherDataService.fetchAreaNamesBeginingWith(anyString())).thenThrow(new ServiceException("error retreiving weather data"));

        //when
      MvcResult mvcResult = this.mockMvc.perform(get("/api/weather/cities/names?namesLike=t")
                .contentType(MediaType.APPLICATION_JSON))
                //then
                .andDo(print())
                .andExpect(status().isServiceUnavailable())
              .andReturn();
      assertThat(mvcResult.getResolvedException().getClass()).isEqualTo(ServiceException.class);
    }

    private Area prepareArea() {
        Area area = new Area();
        area.setId("1333");
        area.setTemperature(13.0);
        area.setName("Test");
        return area;
    }
}