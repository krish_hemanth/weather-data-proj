package com.weather.api.service;

import com.weather.api.exception.ServiceException;
import com.weather.api.model.Area;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FetchWeatherDataServiceTest {

    @InjectMocks
    private FetchWeatherDataService fetchWeatherDataService;

    @Mock
    private ClientBuilder clientBuilder;

    @Test
    public void testFetchAreaNamesBeginingWith_foundMatch() throws Exception {
        when(clientBuilder.doGet(Mockito.any())).thenReturn("{\"cod\":\"200\",\"calctime\":0.3107,\"cnt\":15,\"list\":[{\"id\":2208791,\"name\":\"Yafran\",\"coord\":{\"lon\":12.52859,\"lat\":32.06329},\"main\":{\"temp\":9.68,\"temp_min\":9.681,\"temp_max\":9.681,\"pressure\":961.02,\"sea_level\":1036.82,\"grnd_level\":961.02,\"humidity\":85},\"dt\":1485784982,\"wind\":{\"speed\":3.96,\"deg\":356.5},\"rain\":{\"3h\":0.255},\"clouds\":{\"all\":88},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}]}]}");

        List<Area> areaList = fetchWeatherDataService.fetchAreaNamesBeginingWith("y");
        assertEquals(areaList.size(), 1);
        assertEquals(areaList.get(0).getName(), "Yafran");
        assertEquals(areaList.get(0).getLatitude(), 32.06329);
        assertEquals(areaList.get(0).getLongitude(), 12.52859);
    }

    @Test
    public void testFetchAreaNamesBeginingWith_noMatches() throws Exception {
        when(clientBuilder.doGet(Mockito.any())).thenReturn("{\"cod\":\"200\",\"calctime\":0.3107,\"cnt\":15,\"list\":[{\"id\":2208791,\"name\":\"Yafran\",\"coord\":{\"lon\":12.52859,\"lat\":32.06329},\"main\":{\"temp\":9.68,\"temp_min\":9.681,\"temp_max\":9.681,\"pressure\":961.02,\"sea_level\":1036.82,\"grnd_level\":961.02,\"humidity\":85},\"dt\":1485784982,\"wind\":{\"speed\":3.96,\"deg\":356.5},\"rain\":{\"3h\":0.255},\"clouds\":{\"all\":88},\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}]}]}");

        List<Area> areaList = fetchWeatherDataService.fetchAreaNamesBeginingWith("t");
        assertEquals(areaList.size(), 0);
    }

    @Test(expected = ServiceException.class)
    public void testFetchAreaNamesBeginingWith_exceptionFetchingData() throws Exception {
        when(clientBuilder.doGet(Mockito.any())).thenThrow(new ServiceException("error fetching data"));

        fetchWeatherDataService.fetchAreaNamesBeginingWith("t");
    }
}